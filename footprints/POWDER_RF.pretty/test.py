from intersect import Point, Segment

a = Point(1, 2)
b = Point(3, 4)
c = Point(5,6.04)
d = Point(7,8.1)

ab = Segment(a,b)
cd = Segment(c,d)

print(ab.intersect(cd))

from __future__ import division

class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __repr__(self):
        return "Point({x}, {y})".format(x=self.x, y=self.y)

    def __str__(self):
        return "({x}, {y})".format(x=self.x, y=self.y)

class Segment(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def delta(self):
        return self.end.x - self.start.x, self.end.y - self.start.y

    def intersect(self, other):
        x1, y1 = self.start.x, self.start.y
        x2, y2 = self.end.x, self.end.y
        x3, y3 = other.start.x, other.start.y
        x4, y4 = other.end.x, other.end.y

        try:
            t = (((x1-x3)*(y3-y4) - (y1-y3)*(x3-x4)) /
                 ((x1-x2)*(y3-y4) - (y1-y2)*(x3-x4)))
        except ZeroDivisionError:
            raise Exception("Segments parallel, no intersection")

        delta_x, delta_y = self.delta()

        result = Point(self.start.x + t * delta_x,
                       self.start.y + t * delta_y)

        return result, t

from __future__ import division

from intersect import Point, Segment

top_bottom_shield_template = """
  (pad 5 smd custom (at 0 {pcb_h_l}) (size 0.01 0.01) (layers In1.Cu Eco1.User)
    (zone_connect 2)
    (options (clearance outline) (anchor circle))
    (primitives
      (gr_poly (pts
         (xy {x10_2} 0)
         (xy {x10_2} {z10_3_cut})
         (xy {ip_x} {ip_z_3})
         (xy {x7} {z7_3})
         (xy {x6} {z6_3})
         (xy {x5} {z5_3})
         (xy {x4} {z4_3})
         (xy {x3} {z3_3})
         (xy {x2} {z2_3})
         (xy {x1} {z1_3})
         (xy 0 {z0_3})
         (xy -{x1} {z1_3})
         (xy -{x2} {z2_3})
         (xy -{x3} {z3_3})
         (xy -{x4} {z4_3})
         (xy -{x5} {z5_3})
         (xy -{x6} {z6_3})
         (xy -{x7} {z7_3})
         (xy -{ip_x} {ip_z_3})
         (xy -{x10_2} {z10_3_cut})
         (xy -{x10_2} 0)
         )
         (width 0.0))
    ))
"""

left_right_shield_template = """
  (pad 5 smd custom (at {x_outer} 0) (size 0.01 0.01) (layers In1.Cu Eco1.User)
    (zone_connect 2)
    (options (clearance outline) (anchor circle))
    (primitives
      (gr_poly (pts
        (xy {x_s_1} {pcb_h_l})
        (xy {x_s_1} {y_s_1})
        (xy {x_s_2} 0)
        (xy {x_s_1} -{y_s_1})
        (xy {x_s_1} -{pcb_h_l})
        (xy 0 -{pcb_h_l})
        (xy 0 {pcb_h_l})
      )
      (width 0.0))
    ))
"""

port_template = """
  (pad {port} smd custom (at {offset_x} {pcb_h_l}) (size 0.1 0.1) (layers In1.Cu Eco1.User)
    (zone_connect 0)
    (options (clearance outline) (anchor circle))
    (primitives
      (gr_poly (pts
         (xy {x10_3} 0)
         (xy {x10_3} {z10_3})
         (xy {ip2_x} {ip2_z_2})
         (xy {x8} {z8_2})
         (xy {x7} {z7_2})
         (xy {x6} {z6_2})
         (xy {x5} {z5_2})
         (xy {x4} {z4_2})
         (xy {x3} {z3_2})
         (xy {x2} {z2_2})
         (xy {x1} {z1_2})
         (xy {offset_x_neg} {z0_2})
         (xy {offset_x_neg} {z0})
         (xy {x1} {z1})
         (xy {x2} {z2})
         (xy {x3} {z3})
         (xy {x4} {z4})
         (xy {x5} {z5})
         (xy {x6} {z6})
         (xy {x7} {z7})
         (xy {x8} {z8})
         (xy {x9} {z9})
         (xy {x10_3} {z10})
         (xy {x10} {z10_2})
         (xy {x10} 0)
         )
         (width 0.0))
    ))
"""

template = """
(module coupler_1 (layer F.Cu) (tedit 5C58DA74)
  (fp_text reference REF** (at 0 0.5) (layer F.SilkS)
    (effects (font (size 1 1) (thickness 0.15)))
  )
  (fp_text value coupler_1 (at 0 -0.5) (layer F.Fab)
    (effects (font (size 1 1) (thickness 0.15)))
  )
  (fp_text user "strip width, space: {strip_w}, {space_w}" (at 0 {pcb_h_l}) (layer Cmts.User)
    (effects (font (size 1 1) (thickness 0.15)))
  )
{ports}
  (pad 5 thru_hole circle
    (at -{offset_x} 0.0)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at {offset_x} 0.0)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -6.2 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -5 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -4 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -3 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -2 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -1 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 0 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 1 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 2 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 3 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 4 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 5 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 6.2 -{pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -6.2 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -5 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -4 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -3 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -2 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -1 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 0 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 1 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 2 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 3 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 4 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 5 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 6.2 {pcb_h_l})
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 5)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 4)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 3)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 2)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 9 1)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 9 -1)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 -2)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 -3)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 -4)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at 10 -5)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 5)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 4)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 3)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 2)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -9 1)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -9 -1)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 -2)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 -3)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 -4)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
  (pad 5 thru_hole circle
    (at -10 -5)
    (size {via_size} {via_size})
    (drill {min_drill})
    (layers *.Cu *.Mask)
    (zone_connect 2)
  )
)
"""

params = {
        'annular_ring': 0.1016,
        'coupler_space_w': 0.6,
        'coupler_strip_w': 1.6,
        'inner_corner_cut': 0.5,
        'min_drill': 0.254,
        'offset_x': 25/3,
        'pcb_l': 10,
        'space_w': 0.35,
        'strip_w': 1.4,

        'x1':  2.5/3,
        'x2':  5.0/3,
        'x3':  7.5/3,
        'x4': 10.0/3,
        'x5': 12.5/3,
        'x6': 15.0/3,
        'x7': 17.5/3,
        'x8': 20.0/3,
        'x9': 22.5/3,

        'z0': 0.067,
        'z1': 0.14/2,
        'z2': 0.19/2,
        'z3': 0.25/2,
        'z4': 0.32/2,
        'z5': 0.42/2,
        'z6': 0.60/2,
        'z7': 0.80/2,
        'z8': 1.00/2,
        'z9': 1.20/2,
        'z10': 1.20,
        'z10_2': 3,
}

params.update({
        'x10': params["offset_x"] + params["strip_w"] / 2,
        'x10_2': params["offset_x"] - params["strip_w"] / 2 -
            params["space_w"],
        'x10_3': params["offset_x"] - params["strip_w"] / 2,

        'z0_2': params["z0"] + params["coupler_strip_w"]*0.9,
        'z1_2': params["z1"] + params["coupler_strip_w"]*0.9,
        'z2_2': params["z2"] + params["coupler_strip_w"]*0.92,
        'z3_2': params["z3"] + params["coupler_strip_w"]*0.96,
        'z4_2': params["z4"] + params["coupler_strip_w"]*1.0,
        'z5_2': params["z5"] + params["coupler_strip_w"]*1.05,
        'z6_2': params["z6"] + params["coupler_strip_w"]*1.15,
        'z7_2': params["z7"] + 1.9,
        'z8_2': params["z8"] + 2.0,
        'z9_2': params["z9"] + 2.1,
})

params.update({
        'z0_3': params["z0_2"] + params["coupler_space_w"],
        'z1_3': params["z1_2"] + params["coupler_space_w"],
        'z2_3': params["z2_2"] + params["coupler_space_w"],
        'z3_3': params["z3_2"] + params["coupler_space_w"],
        'z4_3': params["z4_2"] + params["coupler_space_w"],
        'z5_3': params["z5_2"] + params["coupler_space_w"],
        'z6_3': params["z6_2"] + params["coupler_space_w"],
        'z7_3': params["z7_2"] + params["coupler_space_w"] * 1.1,
        'z8_3': params["z8_2"] + params["coupler_space_w"] * 1.2,
        'z9_3': params["z9_2"] + params["coupler_space_w"] * 1.3,
        'z10_3': params["z10_2"] + params["coupler_space_w"],
})

pcb_h_l = params["pcb_l"] / 2
via_size = params["min_drill"] + 2*params["annular_ring"]
x_s_1 = params["offset_x"] + params["strip_w"] / 2 + params["space_w"]
x_s_2 = params["offset_x"] - params["strip_w"] / 2 - params["space_w"]
y_s_1 = params["strip_w"] + 2*params["space_w"]
x_outer = x_s_1 + 1
z10_3_cut = params["z10_3"] + params["inner_corner_cut"]

a = Point(params["x7"], params["z7_3"])
b = Point(params["x8"], params["z8_3"])
c = Point(params["x10_2"], z10_3_cut)
d = Point(params["x10_2"]-params["inner_corner_cut"], params["z10_3"])

ab = Segment(a, b)
cd = Segment(c, d)

ipoint, t = ab.intersect(cd)
assert 0 <= t <= 1
params["ip_x"] = ipoint.x
params["ip_z_3"] = ipoint.y

print(ab.intersect(cd))

a = Point(params["x8"], params["z8_3"])
b = Point(params["x9"], params["z9_3"])
c = Point(params["x10_2"], z10_3_cut)
d = Point(params["x10_2"]-params["inner_corner_cut"], params["z10_3"])

ab = Segment(a, b)
cd = Segment(c, d)

print(ab.intersect(cd))

a = Point(params["x9"], params["z9_3"])
b = Point(params["x10_2"], params["z10_3"])
c = Point(params["x10_2"], z10_3_cut)
d = Point(params["x10_2"]-params["inner_corner_cut"], params["z10_3"])

ab = Segment(a, b)
cd = Segment(c, d)

print(ab.intersect(cd))

a = Point(params["x9"], params["z9_2"])
b = Point(params["x10_3"], params["z10_2"])
c = Point(params["x10_3"], params["z10_3"])
d = Point(params["x10_2"], params["z9_2"])

ab = Segment(a, b)
cd = Segment(c, d)

#ipoint, t = ab.intersect(cd)
#assert 0 <= t <= 1

print(ab.intersect(cd))

a = Point(params["x8"], params["z8_2"])
b = Point(params["x9"], params["z9_2"])
c = Point(params["x10_3"], params["z10_3"])
d = Point(params["x10_2"], params["z9_2"])

ab = Segment(a, b)
cd = Segment(c, d)

ipoint, t = ab.intersect(cd)
assert 0 <= t <= 1

params["ip2_x"] = ipoint.x
params["ip2_z_2"] = ipoint.y

print(ab.intersect(cd))

x_keys = ["x1",
          "x2",
          "x3",
          "x4",
          "x5",
          "x6",
          "x7",
          "x8",
          "x9",
          "x10",
          "x10_2",
          "x10_3",
          "ip_x",
          "ip2_x",
         ]

y_keys = ["z0",
          "z1",
          "z2",
          "z3",
          "z4",
          "z5",
          "z6",
          "z7",
          "z8",
          "z9",
          "z10",
          "z10_2",
          'z0_2',
          'z1_2',
          'z2_2',
          'z3_2',
          'z4_2',
          'z5_2',
          'z6_2',
          'z7_2',
          'z8_2',
          'z9_2',
          'z0_3',
          'z1_3',
          'z2_3',
          'z3_3',
          'z4_3',
          'z5_3',
          'z6_3',
          'z7_3',
          'z8_3',
          'z9_3',
          'z10_3',
          "ip2_z_2",
          "ip_z_3",
         ]

# ++
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= params["offset_x"]
for y_key in y_keys:
    params2[y_key] -= pcb_h_l

ports = port_template.format(pcb_h_l=pcb_h_l,
                             via_size=via_size,
                             x_s_1=x_s_1-params["offset_x"],
                             x_s_2=x_s_2-params["offset_x"],
                             y_s_1=y_s_1-pcb_h_l,
                             x_outer=x_outer-params["offset_x"],
                             z10_3_cut=z10_3_cut-pcb_h_l,
                             offset_x_neg=-params["offset_x"],
                             port=2,
                             **params2)

# +-
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= params["offset_x"]
for y_key in y_keys:
    params2[y_key] -= pcb_h_l
    params2[y_key] = -params2[y_key]

ports += port_template.format(pcb_h_l=-pcb_h_l,
                              via_size=via_size,
                              x_s_1=x_s_1-params["offset_x"],
                              x_s_2=x_s_2-params["offset_x"],
                              y_s_1=-(y_s_1-pcb_h_l),
                              x_outer=x_outer-params["offset_x"],
                              z10_3_cut=-(z10_3_cut-pcb_h_l),
                              offset_x_neg=-params["offset_x"],
                              port=4,
                              **params2)

# -+
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= params["offset_x"]
    params2[x_key] = -params2[x_key]
for y_key in y_keys:
    params2[y_key] -= pcb_h_l

params2["offset_x"] = -params2["offset_x"]

ports += port_template.format(pcb_h_l=pcb_h_l,
                              via_size=via_size,
                              x_s_1=-(x_s_1-params["offset_x"]),
                              x_s_2=-(x_s_2-params["offset_x"]),
                              y_s_1=y_s_1-pcb_h_l,
                              x_outer=-(x_outer-params["offset_x"]),
                              z10_3_cut=z10_3_cut-pcb_h_l,
                              offset_x_neg=params["offset_x"],
                              port=1,
                              **params2)

# --
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= params["offset_x"]
    params2[x_key] = -params2[x_key]
for y_key in y_keys:
    params2[y_key] -= pcb_h_l
    params2[y_key] = -params2[y_key]

params2["offset_x"] = -params2["offset_x"]

ports += port_template.format(pcb_h_l=-pcb_h_l,
                              via_size=via_size,
                              x_s_1=-(x_s_1-params["offset_x"]),
                              x_s_2=-(x_s_2-params["offset_x"]),
                              y_s_1=-(y_s_1-pcb_h_l),
                              x_outer=-(x_outer-params["offset_x"]),
                              z10_3_cut=-(z10_3_cut-pcb_h_l),
                              offset_x_neg=params["offset_x"],
                              port=3,
                              **params2)


# top_bottom_shield

# ++
params2 = dict(params)
for y_key in y_keys:
    params2[y_key] -= pcb_h_l

ports += top_bottom_shield_template.format(pcb_h_l=pcb_h_l,
                                           via_size=via_size,
                                           x_s_1=x_s_1,
                                           x_s_2=x_s_2,
                                           y_s_1=y_s_1-pcb_h_l,
                                           x_outer=x_outer,
                                           z10_3_cut=z10_3_cut-pcb_h_l,
                                           offset_x_neg=-params["offset_x"],
                                           **params2)

# +-
params2 = dict(params)
for y_key in y_keys:
    params2[y_key] -= pcb_h_l
    params2[y_key] = -params2[y_key]

ports += top_bottom_shield_template.format(pcb_h_l=-pcb_h_l,
                                           via_size=via_size,
                                           x_s_1=x_s_1,
                                           x_s_2=x_s_2,
                                           y_s_1=-(y_s_1-pcb_h_l),
                                           x_outer=x_outer,
                                           z10_3_cut=-(z10_3_cut-pcb_h_l),
                                           offset_x_neg=-params["offset_x"],
                                           **params2)


# left_right_shield

# ++
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= x_outer

ports += left_right_shield_template.format(pcb_h_l=pcb_h_l,
                                           via_size=via_size,
                                           x_s_1=x_s_1-x_outer,
                                           x_s_2=x_s_2-x_outer,
                                           y_s_1=y_s_1,
                                           x_outer=x_outer,
                                           z10_3_cut=z10_3_cut,
                                           offset_x_neg=-params["offset_x"],
                                           **params2)

# -+
params2 = dict(params)
for x_key in x_keys:
    params2[x_key] -= x_outer
    params2[x_key] = -params2[x_key]

params2["offset_x"] = -params2["offset_x"]

ports += left_right_shield_template.format(pcb_h_l=pcb_h_l,
                                            via_size=via_size,
                                            x_s_1=-(x_s_1-x_outer),
                                            x_s_2=-(x_s_2-x_outer),
                                            y_s_1=y_s_1,
                                            x_outer=-x_outer,
                                            z10_3_cut=z10_3_cut,
                                            offset_x_neg=params["offset_x"],
                                            **params2)


with open("coupler_1.kicad_mod", "w") as out_file:
    out_file.write(template.format(pcb_h_l=pcb_h_l,
                                   via_size=via_size,
                                   x_s_1=x_s_1,
                                   x_s_2=x_s_2,
                                   y_s_1=y_s_1,
                                   x_outer=x_outer,
                                   z10_3_cut=z10_3_cut,
                                   ports=ports,
                                   **params))

import decimal

for i in range(20):
  print(f"(pad {2*i+1} smd rect (at {decimal.Decimal('15.2')/2-i*decimal.Decimal('0.8')} 6.275) (size 0.5 1.25) (layers F.Cu F.Paste F.Mask))")
  print(f"(pad {2*i+2} smd rect (at {decimal.Decimal('15.2')/2-i*decimal.Decimal('0.8')} 8.03) (size 0.5 1.5) (layers F.Cu F.Paste F.Mask))")

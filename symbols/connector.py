p1="X Pin_1 1 -200 1900 150 R 50 50 1 1 P"
p2="X Pin_2 2 300 1900 150 L 50 50 1 1 P"
p10="X Pin_10 10 300 1500 150 L 50 50 1 1 P"

def pin_string(pin_number, total_pins):
    x = -200 if pin_number % 2 == 1 else 300
    y = -100 * ((pin_number + 1) // 2)
    y += 100 * (total_pins // 4)
    lr = "R" if pin_number % 2 == 1 else "L"

    return f"X Pin_{pin_number} {pin_number} {x} {y} 150 {lr} 50 50 1 1 P"

print(pin_string(1, 80) == p1, pin_string(1, 80))
print(pin_string(2, 80) == p2, pin_string(2, 80))
print(pin_string(10, 80) == p10, pin_string(10, 80))

for i in range(140):
    print(pin_string(i+1, 140))
